---
title: Anaconda Installation
sidebar: mydoc_sidebar
permalink: mydoc_Installation.html
folder: mydoc
---

The easiest way to install Snakemake (and other bioinformatics software) is via *conda*, which you get through Anaconda or miniconda.

```
wget https://repo.anaconda.com/archive/Anaconda3-5.3.1-Linux-x86_64.sh
bash Anaconda3-5.3.1-Linux-x86_64.sh
```

Or for miniconda run

```
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
```

Installing conda



